# Changelog

## 0.0.7

- fix handling of Option

## 0.0.6

- Strong mode stuff

## 0.0.3 

- Dynamic JSON

## 0.0.1

- Initial version, created by Stagehand
