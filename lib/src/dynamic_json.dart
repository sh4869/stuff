// Copyright (c) 2015, Anders Holmgren. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

library stuff.json.dynamic;

import 'dart:mirrors';

import 'package:stuff/src/json_utils.dart';

class DynamicJson implements Jsonable {
  final Map<String, Object> _json;

  DynamicJson.fromJson(this._json);

  Map<String, Object> toJson() => _json;

  parseSingle(String fieldName, [create(i)]) =>
      new JsonParser(_json, false).single(fieldName, create);

  noSuchMethod(Invocation i) {
    if (i.isGetter) {
      return _json[MirrorSystem.getName(i.memberName)];
    } else {
      return super.noSuchMethod(i);
    }
  }
}
